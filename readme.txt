==== WP_Plugin ====
Contributors: davi2206
Requires at least: 4.8
Tested up to: 5.3.2
Requires PHP: 7
Stable Tag: 5.0.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

A small plugin with custom shortcodes and usefull functions. 


==== Description ====
A small plugin with custom shortcodes for inserting various useful information in your WP site
and more. 


==== Functions ====
Shortcodes:
- Years: [years date="dd-mm-YYYY"], displays the number of years since the given date. Can fx. be used to calculate age
- Rand_post: [rand_post], displays a random post from the page
- Archive: [archive type="type" limit="n" show_count="bool" show_older="bool" older_txt=".." format="..."], displays the archive links formated as requested
	type: yearly, monthly, weekly, daily
	limit: How many archives to get
	show_count: show number of posts in archive
	show_older: Show "Older" link to /Archive page
	older_txt: Archive link Text
	format: the format of the shown archive
- Sitename: [sitename], displays the name of the site
- Scheduled posts: [scheduled_posts days=#], displays the number of scheduled posts, from current date, going forware the amout of days specified
- Last post time: [last_posted], display the time since the latest post

==== Frequently Asked Questions ====
None so far. 


==== Changelog ====
= 5.0.0 =
* Removed JS stuff, since it turns out to be pointles

= 4.9.0 =
* Adding custom JS from shortcode instead of page name

= 4.8.0 =
* Adding custom JS on specific page

= 4.7.0 =
* Adding Last Post Time shortcode

= 4.6.0 =
* Adding 'days' parameter to Scheduled posts shortcode

= 4.5.0 =
* Added Scheduled posts shortcode
* Fixed plugin website information

= 4.4.0 =
* Added Sitename shortcode

= 4.3.0 =
* Hiding menues from Contributors

= 4.2.0 =
* Updated formating on shortcode "archive"

= 4.1.0 =
* Added parameters to shortcode "archive"

= 4.0.0 =
* Added shortcode "archive"

= 3.1.0 =
* Reworked "years" shortcode
* Cleaned up readme.txt

= 3.0.1 =
* Added shortcode "rand_post"
* Removed shortcode "files"

= 3.0 =
* Added shortcode "files"

= 2.0 =
* Added function 'user_as_post_category'

= 1.0 =
* First version. Never published