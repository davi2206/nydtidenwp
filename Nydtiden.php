<?php
/*
Plugin Name: WP_Plugin
Description: Custom shortcodes and more
Plugin URI: https://TheSlowLoris.com
Version: 5.0.0
Author: Davi2206
Author URI: https://TheSlowLoris.com/about
License: GPLv2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
*/
	/* Calculate years from date */
	function get_years($atts) {
		$a = shortcode_atts( array(
				'date' => '-1',
		), $atts );
		
		$d = DateTime::createFromFormat('d-m-Y', $a['date']);
    	
		//Date Format
		$dateform = $d && $d->format('d-m-Y') === $a['date'];

		if($dateform){
			$years = date("Y", (time()- strtotime($a['date']))) - 1970;
			return $years;
		}
		else {
			return "**Broken date format: Expected 'dd-mm-YYYY' got {$a['date']}**";
		}
	}
	add_shortcode('years', 'get_years');
	
	/* Selects a random post from the site */
	function get_random_post() {
		$args = array(
			'post_type' => 'post',
			'orderby'   => 'rand',
			'posts_per_page' => 5,
		);
		$the_query = new WP_Query( $args );
		
		if ( $the_query->have_posts() ) {
			while ( $the_query->have_posts() ) {
				$the_query->the_post();
				$link = get_permalink();
			}
			/* Restore original Post Data */
			wp_reset_postdata();
			wp_redirect($link,307);
               exit;
		}
		else {}
	}
	add_shortcode('rand_post', 'get_random_post');
	
	/* Get the selected amount and type of archive links */
	function get_archive($args) {
		$atts = shortcode_atts( array(
				'type' => 'monthly',
				'limit' => '5',
				'show_count' => '0',
				'show_older' => '0',
				'older_txt' => 'Older',
				'format' => 'html',
		), $args );
		
		$type = $atts['type'];
		$limit = $atts['limit'];
		$show_count = $atts['show_count'];
		$format = $atts['format'];
		
		$my_archives = wp_get_archives(array(
			'type' => $atts['type'], 
			'limit' => $atts['limit'],
			'show_post_count' => $atts['show_count'],
			'format' => $atts['format'],
			'echo' => '0'
		));
		
		if ( $atts['show_older'] === "1" ) {
			$my_archives .=  '<li><a href="/archive">' . $atts['older_txt'] . '</a>&nbsp;</li>';
		}
		
		return $my_archives; 
	}
	add_shortcode('archive', 'get_archive');
	
	/* Display the site name */
	function get_site_name() {
		$args = array();
		$sitename = get_bloginfo( 'name' );
		
		return $sitename;
	}
	add_shortcode('sitename', 'get_site_name');
	
	/* Display nr of scheduled posts */
	function get_scheduled_posts($args) {
		$atts = shortcode_atts( array(
				'days' => '0',
		), $args );
		
		$additions = 'P'.$atts['days'].'D';
		
		$date = new DateTime();
		$date->add(new DateInterval($additions));
		$laterdate = $date->format('Y-m-d');
		
		$args = array(
			'post_status' => 'future',
			'date_query' => array(
				array(
					'after'		=> $today,
					'before'	=> $laterdate,
				),
			),
		);
		$the_query = new WP_Query( $args );
		return $the_query->found_posts;
	}
	add_shortcode('scheduled_posts', 'get_scheduled_posts');
	
	/* Display time since last post */
	function get_last_post_age($args) {
		$now = time();
		$last_post = strtotime(get_lastpostdate());
		$diff_sec = ($now-$last_post);
		$diff_days = ($diff_sec/(60*60*24));
		$datediff = floor($diff_days);
		return $datediff;
	}
	add_shortcode('last_posted', 'get_last_post_age');

	
	// Enable shortcode execution in text widget
	add_filter('widget_text', 'do_shortcode');
	
	// Hide menues for contributers
	function remove_menus() {
		$user = wp_get_current_user();
		if ( in_array( 'contributor', (array) $user->roles ) ) {
			//The user has the "contributor" role
			remove_menu_page( 'index.php' );                  //Dashboard
			remove_menu_page( 'jetpack' );                    //Jetpack* 
			remove_menu_page( 'admin.php?page=jetpack#/' );   //Jetpack 2* 
			remove_menu_page( 'edit.php' );                   //Posts
			remove_menu_page( 'edit.php?post_type=wpdmpro' ); //Downloads
			remove_menu_page( 'upload.php' );                 //Media
			remove_menu_page( 'tools.php' );                  //Tools
		}
	}
	// add_action( 'admin_menu', 'remove_menus' );
?>